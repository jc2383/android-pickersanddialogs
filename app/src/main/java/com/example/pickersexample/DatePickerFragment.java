package com.example.pickersexample;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;

// 1. Extend DialogFragment and implement the DatePickerDialog
public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    // 3. Delete this constructor. It's not required.
//    public DatePickerFragment() {
//        // Required empty public constructor
//    }



    // 4. Delete this, not needed.
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        TextView textView = new TextView(getActivity());
//        textView.setText(R.string.hello_blank_fragment);
//        return textView;
//    }



    // 5. This function runs when the picker view launches
    // Set the default date shown on the picker view to today's date.
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Log.d("JENELLE", "Hello world!");

        // --------
        // Set the default calendar date to today's date.
        // --------
        // a. First, create a calendar object
        final Calendar calendar = Calendar.getInstance();
        // b. Using the Calendar object, get year, month, day
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);


        // Create a new Dialog and show it
        return new DatePickerDialog(getActivity(), this,year, month, day);
    }

    // 2. Required method from the DatePickerDialog interface class
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        // When the person presses "OKAY" on the date picker, run this code
        // - This function is defined in MainActivity.java
        MainActivity activity = (MainActivity) getActivity();
        activity.getDatePickerResults(year, month, day);
    }


}

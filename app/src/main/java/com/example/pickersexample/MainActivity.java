package com.example.pickersexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final String TAG = "JENELLE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // 1. When person presses on DATE button, show a pikcer
    public void dateButtonPressed(View view) {
        Log.d(TAG, "Date button pressed");
        DialogFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "datePicker");
    }

    // 2. Get the results from the date picker
    public void getDatePickerResults(int year, int month, int day) {
        String yyyy = Integer.toString(year);
        String mm = Integer.toString(month);  // January = 0, Feb = 1, March = 2
        String dd = Integer.toString(day);

        String date = yyyy + "-" + mm + "-" + dd;

        Log.d(TAG, "Your date is: " + date);

    }

    public void timeButtonPressed(View view) {
        Log.d(TAG, "Time button pressed");

        // Show the timepicker
        DialogFragment fragment = new TimePickerFragment();
        fragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void getTimePickerResults(int hour, int minute) {
        String h = Integer.toString(hour);          // 0 = 12am, 1 = 1am, 2 = 2am...... 16 = 4pm, 17=5pm, 18=6pm,  etc)
        String m = Integer.toString(minute);

        Log.d(TAG, "The time = " + h + ":" + m);

    }

    // show an alert
    public void alertButtonPressed(View view) {
        Log.d(TAG, "Alert button pressed");

        // Build an alert box
        AlertDialog.Builder alertBox = new AlertDialog.Builder(MainActivity.this);

        // Configure the alert box

        // a. Set title and message
        alertBox.setTitle("Continue");
        alertBox.setMessage("Are you sure you want to continue?");

        // b. Add buttons
        alertBox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User clicked OK button.
                Toast t = Toast.makeText(getApplicationContext(), "You pressed YES!", Toast.LENGTH_SHORT);
                t.show();
            }
        });


        alertBox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User clicked OK button.
                Toast t = Toast.makeText(getApplicationContext(), "You pressed No!", Toast.LENGTH_SHORT);
                t.show();
            }
        });

        alertBox.setNeutralButton("Maybe", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User clicked OK button.
                Toast t = Toast.makeText(getApplicationContext(), "You pressed Maybe!", Toast.LENGTH_SHORT);
                t.show();
            }
        });


        // 3. Show the alert box
        alertBox.show();

    }
}

package com.example.pickersexample;


import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    final String TAG="JEENELLE";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Create a new time picker dialog that is set to 12:30
        return new TimePickerDialog(getActivity(), this, 12, 30, false);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        Log.d(TAG, "Time picker dialog - ok button pressed!");
        MainActivity activity = (MainActivity) getActivity();
        activity.getTimePickerResults(hour, minute);

    }


}
